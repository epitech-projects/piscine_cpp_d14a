#ifndef CLUEDO_H_
#define CLUEDO_H_

#include <string>

namespace Cluedo
{
  class Card
  {
  protected:
    Card();
    virtual ~Card();

  public:
    bool	IsPartOfTheCrime() const;

  protected:
    bool	_isPartOfTheCrime;
  };

  class			Weapon: public Card
  {
  public:
    enum		Name
      {
	Corde,
	Matraque,
	Poignard,
	Cle_Anglaise,
	Chandelier,
	Revolver
      };
    static const Name	PartOfTheCrime;

  public:
    Weapon(Weapon::Name name, bool bearsFingerprints);
    ~Weapon();

  public:
    bool		BearsFingerprints() const;
    Weapon::Name	getName() const;

  private:
    Weapon::Name	_name;
    bool		_bearsFingerprints;
  };

  class			Suspect: public Card
  {
  public:
    enum		Name
      {
	Mlle_Rose,
	Pr_Violet,
	Col_Moutarde,
	Dr_Olive,
	Mme_Leblanc,
	Mme_Pervenche
      };
    static const Name	PartOfTheCrime;

  public:
    Suspect(Suspect::Name name, bool isLying);
    ~Suspect();

  public:
    bool		IsLying() const;
    Suspect::Name	getName() const;

  private:
    Suspect::Name	_name;
    bool		_isLying;
  };

  class			Room: public Card
  {
  public:
    enum		Name
      {
	Cuisine,
	Grand_Salon,
	Veranda,
	Petit_Salon,
	Salle_a_Manger,
	Billard,
	Bibliotheque,
	Bureau,
	Hall
      };
    static const Name	PartOfTheCrime;

  public:
    Room(Room::Name name, bool hasSecretPassage);
    ~Room();

  public:
    bool		HasSecretPassage() const;
    Room::Name		getName() const;

  private:
    Room::Name		_name;
    bool		_hasSecretPassage;
  };

  class Game
  {
  public:
    static bool CaseSolved(Weapon* w[], Suspect* s[], Room* r[]);
  };
}

/* Exceptions */

class			WeaponException
{
public:
  WeaponException(Cluedo::Weapon**);
  ~WeaponException();

private:
  std::string		_message;
  Cluedo::Weapon**	_weapon;
};

class			SuspectException
{
public:
  SuspectException(Cluedo::Suspect* suspect[]);
  ~SuspectException();

private:
  std::string		_message;
  Cluedo::Suspect**	_suspect;
};

class			RoomException
{
public:
  RoomException(Cluedo::Room* room[]);
  ~RoomException();

private:
  std::string		_message;
  Cluedo::Room**	_room;
};

#endif // CLUEDO_H_
