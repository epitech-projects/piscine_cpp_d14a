//
// ex00.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d14a/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 21 16:50:21 2014 Jean Gravier
// Last update Tue Jan 21 22:11:00 2014 Jean Gravier
//

#include "ex00.hh"
#include <string>
#include <iostream>

/* WeaponException */

WeaponException::WeaponException(Cluedo::Weapon* weapon[])
{
  this->_message = "Weapon ne contient pas la bonne carte!";
  this->_weapon = weapon;
}

WeaponException::~WeaponException()
{

}

/* SuspectException */

SuspectException::SuspectException(Cluedo::Suspect* suspect[])
{
  this->_message = "Suspect ne contient pas la bonne carte!";
  this->_suspect = suspect;
}

SuspectException::~SuspectException()
{

}

/* RoomException */

RoomException::RoomException(Cluedo::Room* room[])
{
  this->_message = "Room ne contient pas la bonne carte!";
  this->_room = room;
}

RoomException::~RoomException()
{

}

/* Card */

Cluedo::Card::Card()
{

}

Cluedo::Card::~Card()
{

}

bool		Cluedo::Card::IsPartOfTheCrime() const
{
  return (this->_isPartOfTheCrime);
}


/* Weapon */

Cluedo::Weapon::Weapon(Cluedo::Weapon::Name name, bool bearsFingerprints):
  _name(name), _bearsFingerprints(bearsFingerprints)
{
  if (this->_name == Cluedo::Weapon::PartOfTheCrime)
    this->_isPartOfTheCrime = true;
  else
    this->_isPartOfTheCrime = false;
}

Cluedo::Weapon::~Weapon()
{

}

bool		Cluedo::Weapon::BearsFingerprints() const
{
  return (this->_bearsFingerprints);
}

Cluedo::Weapon::Name	Cluedo::Weapon::getName() const
{
  return (this->_name);
}

/* Suspect */

Cluedo::Suspect::Suspect(Cluedo::Suspect::Name name, bool isLying):
  _name(name), _isLying(isLying)
{
  if (this->_name == Cluedo::Suspect::PartOfTheCrime)
    this->_isPartOfTheCrime = true;
  else
    this->_isPartOfTheCrime = false;
}

Cluedo::Suspect::~Suspect()
{

}

bool		Cluedo::Suspect::IsLying() const
{
  return (this->_isLying);
}

Cluedo::Suspect::Name	Cluedo::Suspect::getName() const
{
  return (this->_name);
}

/* Room */

Cluedo::Room::Room(Cluedo::Room::Name name, bool hasSecretPassage):
  _name(name), _hasSecretPassage(hasSecretPassage)
{
  if (this->_name == this->PartOfTheCrime)
    this->_isPartOfTheCrime = true;
  else
    this->_isPartOfTheCrime = false;
}

Cluedo::Room::~Room()
{

}

bool		Cluedo::Room::HasSecretPassage() const
{
  return (this->_hasSecretPassage);
}

Cluedo::Room::Name	Cluedo::Room::getName() const
{
  return (this->_name);
}

/* Game */

bool		Cluedo::Game::CaseSolved(Weapon*  w[], Suspect* s[], Room* r[])
{
  int		i;
  bool		weaponOk;
  bool		suspectOk;
  bool		roomOk;

  weaponOk = false;
  suspectOk = false;
  roomOk = false;

  for (i = 0; w[i]; ++i)
    {
      if (w[i]->IsPartOfTheCrime())
	{
	  weaponOk = true;
	  break;
	}
    }
  for (i = 0; s[i]; ++i)
    {
      if (s[i]->IsPartOfTheCrime())
	{
	  suspectOk = true;
	  break;
	}
    }
  for (i = 0; r[i]; ++i)
    {
      if (r[i]->IsPartOfTheCrime())
	{
	  roomOk = true;
	  break;
	}
    }
  if (weaponOk && suspectOk && roomOk)
    return (true);
  else if (weaponOk && suspectOk && !roomOk)
    throw new RoomException(r);
  else if (weaponOk && !suspectOk && roomOk)
    throw new SuspectException(s);
  else if (!weaponOk && suspectOk && roomOk)
    throw new WeaponException(w);
  return (false);
}
